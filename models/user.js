var mongoose = require('mongoose');
var Schema = mongoose.Schema;


// SCHEMA
var UserSchema = new Schema({
	sid : {type : String, trim: true, index : true, required : true, unique : true},
	name : {type : String, trim: true, required : true},
	dept : {type : String, trim: true, required : true},
	created : Date
});


// POST MIDDLEWARE
UserSchema.post('save', function(doc) {
  console.log('%s has been saved', doc.name);
});


UserSchema.post('remove', function(doc) {
  console.log('%s has been removed', doc.name);
})


module.exports 	= mongoose.model('User', UserSchema);